import turtle
import random
turtle.hideturtle()

firstturtle = turtle.Turtle()
secondturtle = turtle.Turtle()
thirdturtle = turtle.Turtle()

firstturtle.color('red')
secondturtle.color('green')
thirdturtle.color('blue')

firstturtle.shape('turtle')
secondturtle.shape('turtle')
thirdturtle.shape('turtle')

firstturtle.penup()
secondturtle.penup()
thirdturtle.penup()

firstturtle.setpos(-315,0)
secondturtle.setpos(-315,50)
thirdturtle.setpos(-315,-50)

firstturtle.pendown()
secondturtle.pendown()
thirdturtle.pendown()

def Race():
    firstturtle.forward(random.uniform(500,630))
    secondturtle.forward(random.uniform(500,630))
    thirdturtle.forward(random.uniform(500,630))

def main():
    Race()

if __name__ == '__main__':
    main()
