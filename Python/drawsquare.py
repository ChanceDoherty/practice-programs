import turtle
turtle.showturtle()

def Drawsquare(sidelength = turtle.numinput('','Enter a number: ')):
     turtle.forward(sidelength)
     turtle.left(90)
     turtle.forward(sidelength)
     turtle.left(90)
     turtle.forward(sidelength)
     turtle.left(90)
     turtle.forward(sidelength)
     turtle.left(90)
     turtle.left(30)

def Drawsquare2(sidelength2 = turtle.numinput('','Enter a number: ')):
     turtle.forward(sidelength2)
     turtle.left(90)
     turtle.forward(sidelength2)
     turtle.left(90)
     turtle.forward(sidelength2)
     turtle.left(90)
     turtle.forward(sidelength2)
     turtle.left(90)
     turtle.left(30)

def Drawsquare3(sidelength3 = turtle.numinput('','Enter a number: ')):
     turtle.forward(sidelength3)
     turtle.left(90)
     turtle.forward(sidelength3)
     turtle.left(90)
     turtle.forward(sidelength3)
     turtle.left(90)
     turtle.forward(sidelength3)
     turtle.left(90)
