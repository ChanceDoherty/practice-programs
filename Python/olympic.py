import turtle
turtle.showturtle()

turtle.circle(50)

turtle.penup()
turtle.color("green")
turtle.setpos(50,-50)
turtle.pendown()
turtle.circle(50)

turtle.penup()
turtle.color("red")
turtle.setpos(100,0)
turtle.pendown()
turtle.circle(50)

turtle.penup()
turtle.color("yellow")
turtle.setpos(-50,-50)
turtle.pendown()
turtle.circle(50)

turtle.penup()
turtle.color("blue")
turtle.setpos(-100,0)
turtle.pendown()
turtle.circle(50)

turtle.hideturtle()
