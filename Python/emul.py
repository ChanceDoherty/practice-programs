def emul(a, b):
    product = 0

    while b > 0:
        if b % 2 != 0:
            product = product + a
        a = a*2
        b = b//2
    return product


def main():
    print(emul(17, 22))


if __name__ == '__main__':
    main()
